# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: patigunt <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/08/12 18:09:47 by patigunt          #+#    #+#              #
#    Updated: 2018/08/12 18:10:23 by patigunt         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME=bsq
SRCS=srcs/bsq.c

all:
	gcc $(SRCS) -o $(NAME) -g

clean:
	rm -f *.o

fclean: clean
	rm -f $(NAME)

re: fclean all
