/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   bsq.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: patigunt <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 23:41:13 by patigunt          #+#    #+#             */
/*   Updated: 2018/08/15 23:41:16 by patigunt         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>

int		geth(int fd, char *hl)
{
	int n;
	char c[0];

	n = 0;
	while (read(fd, c, 1) && (*c >= '0' && *c <= '9'))
	{
		n = (n * 10) + (c[0] - '0');
	}
	hl[3] = c[0];
	read(fd, hl, 3);
	return (n);
}

int		getwdth(int fd, char *hl)
{
	char c[0];
	int w;

	w = 0;
	while (read(fd, c, 1) && *c != hl[2])
	{
		w++;
	}
	close(fd);
	return (w);
}

void	skl(int fd, char *hl)
{
	char c[0];

	while (read(fd, c, 1) && *c != hl[2])
	{
	
	}
}

void	printmem(int w, int h, int arr[h][w])
{
	int i;
	int j;

	i = 0;
	j = 0;

	while (i < h)
	{
		while (j < w)
		{
			i++;
			j = 0;
		}
		write(1, "\n", 1);
	}
}

void	print(int w, int h, int arr[h][w], int sq, int x0, int y0, char *hl)
{
	int i;
	int j;

	i = 0;
	j = 0;

	while (i < h)
	{
		while (j < w)
		{
			if ((i >= y0 && i < (y0 + sq)) && (j >= x0 && j < (x0 + sq)))
				write(1, &hl[1], 1);
			else if (arr[i][j] == 0)
				write(1, &hl[0], 1);
			else
				write(1, &hl[3], 1);
			j++;
		}
		i++;
		write(1, &hl[2], 1);
		j = 0;
	}
}

int		check(int y, int x, int wd, int arr[][wd], int d)
{
	int i;
	int j;
	int f;

	i = 1;
	f = 1;
	while (i <= d)
	{
		if (arr[y - i][x] == 0)
		{
			f = 0;
			break ;
		}
		i++;
	}
	j = 1;
	while (j <= d)
	{
		if (arr[y][x - j] == 0)
		{
			f = 0;
			break ;
		}
		j++;
	}
	if (f)
		return (1);
	else
	{
		if (i >= j)
			arr[y][x] = j;
		else
			arr[y][x] = i;
		return (0);
	}
}

int		main(int ac, char **av)
{
	int ai;

	int d[ac - 1];
	int h;
	int wd;

	ai = 1;
	while (ai != ac)
	{
		char hl[4];
		d[ai] = open(av[ai], O_RDONLY);
		h = geth(d[ai], hl);
		wd = getwdth(d[ai], hl);
		d[ai] = open(av[ai], O_RDONLY);
		int arr[h][wd];
		int i;
		int max;
		int x0;
		int y0;
		int p;
		int row;
		int col;
		char c[0];
		skl(d[ai], hl);

		i = 0;
		max = 0;
		while (read(d[ai], c, 1))
		{
			row = i / wd;
			col = i % wd;
			arr[row][col] = 0;
			if (c[0] == hl[3])
			{
				if ((row == 0) || (col == 0))
					arr[row][col] = 1;
				else
				{
					p = arr[row - 1][col - 1];
					if (check(row, col, wd, arr, p) && p)
					{
						arr[row][col] = arr[row - 1][col - 1] + 1;
						if (arr[row][col] > max)
						{
							max = arr[row][col];
							y0 = row - max + 1;
							x0 = col - max + 1;
						}
					}
					else
					{
						if (arr[row][col] < 1)
						{
							arr[row][col] = 1;
						}
					}
				}
				i++;
			}
			if (c[0] == hl[0])
			{
				arr[row][col] = 0;
				i++;
			}
		}
		close(d[ai]);
		print(wd, h, arr, max, x0, y0, hl);
		ai++;
	}
	return (0);
}
